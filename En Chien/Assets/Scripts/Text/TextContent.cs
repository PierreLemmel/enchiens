using System.Collections.Generic;
using UnityEngine;

namespace EnChiens.Text
{
    public class TextContent : MonoBehaviour
    {
        [SerializeField]
        private List<TextSpan> spans;
        public IReadOnlyCollection<TextSpan> Spans => spans;
    }
}
