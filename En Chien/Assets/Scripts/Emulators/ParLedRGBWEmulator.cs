﻿using EnChiens.Dmx.Fixtures;
using UnityEngine;

namespace EnChiens.Emulators
{
    public class ParLedRGBWEmulator : LightEmulator<ParLedRGBW>
    {
        protected override Color32 GetColor() => fixture.color;
        protected override int GetIntensity() => fixture.dimmer;
        protected override int GetStrobe() => fixture.stroboscope;
    }
}